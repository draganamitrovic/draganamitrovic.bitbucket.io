//owlCarousel
$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        nav: false,
        pagination: true,
        autoplay: true,
        autoplayTimeout: 1500,
        dotsContainer: false,
        responsive: {
            // breakpoint from 0 up
            0: {
                items: 1,
            },
            // breakpoint from 425 up
            425: {
                items: 2,
            },
            // breakpoint from 768px up
            768: {
                items: 3,
            },
            // breakpoint from 992px up
            992: {
                items: 4,
            },
            // // breakpoint from 1200px up
            // 1200: {
            //     items: 5,
            // }

        }
    });
});

$('body').scrollspy({ target: ".navbar" });

$('#myTab a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
})