const idLenght = 1;

$(document).ready(function () {
    if (localStorage.getItem('cart')) {
        renderTable();
    } else {
        $('#cart-order').remove();
        $('#message').append('<p class="info-message">Vaša korpa je prazna.</p>')
    }
});

function renderTable() {
    var cartTable = document.getElementById('table-body');
    cartTable.innerHTML = '';
    var products = getCartLocalStorage();

    products.forEach(el => {
        var column = document.createElement('tr');

        var producttd = document.createElement('td');
        var name = document.createTextNode(el.name.toUpperCase());
        producttd.appendChild(name);

        var pricetd = document.createElement('td');
        var price = document.createTextNode(parseFloat(el.price).toFixed(2) + ' RSD');
        pricetd.appendChild(price);

        var quantitytd = document.createElement('td');
        var quantitydiv = document.createElement('div');
        quantitydiv.classList.add('form-group');
        quantitytd.appendChild(quantitydiv);
        var quantityinput = document.createElement('input');
        quantityinput.classList.add('form-control');
        quantityinput.setAttribute('type', 'number');
        quantityinput.setAttribute('value', el.quantity);
        quantityinput.setAttribute('id', 'quantityId' + el.id);
        quantityinput.addEventListener("change", function () { changequantity(this.id, this.value) }, true);
        quantitydiv.appendChild(quantityinput);

        var subtotaltd = document.createElement('td');
        var subtotal = document.createTextNode(parseFloat(el.price * el.quantity).toFixed(2) + ' RSD');
        subtotaltd.setAttribute('id', 'subtotal' + el.id)
        subtotaltd.appendChild(subtotal);

        var remove = document.createElement('td');
        var button = document.createElement('button');
        button.setAttribute('type', 'button');
        button.classList.add('btn', 'btn-danger');
        button.setAttribute('id', 'remove' + el.id);
        button.addEventListener("click", function () { confirmWindow(this.id) }, true);
        var btnText = document.createTextNode('Remove');
        button.appendChild(btnText);
        remove.appendChild(button);

        column.appendChild(producttd);
        column.appendChild(pricetd);
        column.appendChild(quantitytd);
        column.appendChild(subtotaltd);
        column.appendChild(remove);

        cartTable.appendChild(column);
    });

    $('#price-total').text(parseFloat(getTotal()).toFixed(2))
}

function getTotal() {
    var total = 0;
    var products = getCartLocalStorage();
    products.forEach(el => {
        var price = el.quantity * el.price;
        total = total + price;
    });

    return total;
}

function getProduct(id) {
    var product = _.where(getAllProducts(), { id: id });
    var productCart = { id: product[0].id, name: product[0].name, quantity: 1, price: product[0].price };
    return productCart;
}

function changequantity(stringId, value) {
    var loader = loadingOverlay.activate();
    var id = Number(stringId.substr(-idLenght));
    console.log('quan', stringId, id, value);
    var product = getProduct(id);
    //find it, find its price*
    var element = document.getElementById('subtotal' + id);
    //change value, 
    var subtotal = value * product.price;
    //insert into storage
    var cart = [];
    async function promise() {
        return new Promise((resolve, reject) => {
            cart = getCartLocalStorage()
        })
    }

    promise()
        .then(
            cart = cart.map(el => {
                if (el.id == id) {
                    el.quantity = value;
                    return el;
                } else {
                    return el;
                }
            }))
        .then(
            localStorage.setItem('cart', JSON.stringify(cart)))
        .then(
            //change html of subtotal*
            element.innerHTML = parseFloat(subtotal).toFixed(2) + ' RSD')
        .then(
            $('#price-total').text(parseFloat(getTotal()).toFixed(2)))
        .then(
            setTimeout(function () {
                loadingOverlay.cancel(loader)
            }, 500)
        )
}

function setLocalStorage(cartIds, cart) {
    console.log(cartIds, cart);
    removeLocalStorage()
        .then(
            localStorage.setItem('cartIds', cartIds))
        .then(
            localStorage.setItem('cart', JSON.stringify(cart)))
}

async function removeLocalStorage() {
    return new Promise((resolve, reject) => {
        localStorage.removeItem('cartIds');
        localStorage.removeItem('cart');
    });
}

function getCartIdsLocalStorage() {
    var cartIds = localStorage.getItem('cartIds').split(',');
    return cartIds;
}

function getCartLocalStorage() {
    var cart = JSON.parse(localStorage.getItem('cart'));
    return cart;
}

function confirmWindow(idString) {
    var confirm = window.confirm("Are you shure you want to remove this item from cart.");
    if (confirm) {
        removeFromCart(idString);
    }
}

function removeFromCart(idString) {
    var id = Number(idString.substr(-idLenght));
    console.log(idString, id);
    var cartIds = [];
    var cart = [];
    if (localStorage.getItem('cartIds')) {
        async function promise(params) {
            return new Promise((resolve, reject) => {
                cartIds = getCartIdsLocalStorage();
                cart = getCartLocalStorage();
                console.log(cartIds, cart);
            });
        };
        promise().then(
            cartIds = cartIds.filter(el => {
                if (el != id) {
                    console.log(el, id);

                    return el;
                }
            }))
            .then(
                cart = cart.filter(el => {
                    if (el.id != id) {
                        console.log(el.id, id);
                        return el;
                    }
                }))
            .then(
                setLocalStorage(cartIds, cart))
            .then(
                renderTable()
            )
    }
}

function getAllProducts() {
    var productList = [
        {
            "id": 1002,
            "imgIndex": "./img/packagesOld/Brusnica.jpg",
            "imgSingleProduct": "../img/packagesOld/Brusnica.jpg",
            "name": "Cranberry soap",
            "ime": "Sapun sa brusnicom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This CRANBERRY SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and cranberry essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1003,
            "imgIndex": "./img/packagesOld/Neven i Aktivni ugalj.jpg",
            "imgSingleProduct": "../img/packagesOld/Neven i Aktivni ugalj.jpg",
            "name": "Calendula with activated charcoal soap",
            "ime": "Sapun sa nevenom i aktivnim ugljem",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This CALENDULA WITH ACTIVATED CHARCOAL SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E and calendula oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1004,
            "imgIndex": "./img/packagesOld/Neven, Zelena glina i Zeolit.jpg",
            "imgSingleProduct": "../img/packagesOld/Neven, Zelena glina i Zeolit.jpg",
            "name": "Calendula with green clay and zeolite soap",
            "ime": "Sapun sa nevenom, zelenom glinom i zeolitom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This CALENDULA WITH GREEN CLAY AND ZEOLITE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and peach essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1005,
            "imgIndex": "./img/packagesOld/Breskva.jpg",
            "imgSingleProduct": "../img/packagesOld/Breskva.jpg",
            "name": "Peach soap",
            "ime": "Sapun sa breskvom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This PEACH SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and peach essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1006,
            "imgIndex": "./img/packagesOld/Lavanda.jpg",
            "imgSingleProduct": "../img/packagesOld/Lavanda.jpg",
            "name": "Lavander soap",
            "ime": "Sapun sa lavandom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This LAVANDER SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and lavander essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1007,
            "imgIndex": "./img/packagesOld/",
            "imgSingleProduct": "../img/packagesOld/",
            "name": "Mint soap",
            "ime": "Sapun sa mentom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This MINT SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and mint essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1008,
            "imgIndex": "./img/packagesOld/Borove iglice.jpg",
            "imgSingleProduct": "../img/packagesOld/Borove iglice.jpg",
            "name": "Pine soap",
            "ime": "Sapun sa borovim iglicama",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This PINE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and pine essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1009,
            "imgIndex": "./img/packagesOld/BRada i brkovi sapun.jpg",
            "imgSingleProduct": "../img/packagesOld/BRada i brkovi sapun.jpg",
            "name": "Beard soap",
            "ime": "Sapun za bradu",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This BEARD SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1010,
            "imgIndex": "./img/packagesOld/BRada i brkovi sapun.jpg",
            "imgSingleProduct": "../img/packagesOld/BRada i brkovi sapun.jpg",
            "name": "Beard and moustache soap",
            "ime": "Sapun za bradu i brkove",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This BEARD AND MOUSTACHE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring.",
            "sastojci": "...",
            "useFor": "face, beard, moustache"
        },
        {
            "id": 1011,
            "imgIndex": "./img/packagesOld/BRada i brkovi sapun.jpg",
            "imgSingleProduct": "../img/packagesOld/BRada i brkovi sapun.jpg",
            "name": "Beard soap",
            "ime": "Sapun za bradu",
            "type": "beard",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This BEARD SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring.",
            "sastojci": "...",
            "useFor": "beard"
        },
        {
            "id": 1012,
            "imgIndex": "./img/packagesOld/BRada i brkovi sapun.jpg",
            "imgSingleProduct": "../img/packagesOld/BRada i brkovi sapun.jpg",
            "name": "Beard and moustache soap",
            "ime": "Sapun za bradu i brkove",
            "type": "moustache",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This BEARD AND MOUSTACHE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring.",
            "sastojci": "...",
            "useFor": "moustache"
        },
        {
            "id": 1013,
            "imgIndex": "./img/packagesOld/Brk Sandalovina.jpg",
            "imgSingleProduct": "../img/packagesOld/Brk Sandalovina.jpg",
            "name": "Sandalovina wax",
            "ime": "Vosak sa sandalovinom",
            "type": "moustache",
            "price": 150.00,
            "description": "Handmade oragnic wax with pure essential oil and the finest natural ingredients. This SANDALOVINA WAX is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, sandalovina extract.",
            "sastojci": "...",
            "useFor": "moustache"
        },
        {
            "id": 1014,
            "imgIndex": "./img/packagesOld/Kakao, Cimet, Pomorandza.jpg",
            "imgSingleProduct": "../img/packagesOld/Kakao, Cimet, Pomorandza.jpg",
            "name": "Cocoa, cinnamon, orange soap",
            "ime": "Sapun sa kakaom, cimetom i narandžom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This COCOA, CINNAMON ORANGE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring, cocoa oil, cinnamon, orange enssential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1015,
            "imgIndex": "./img/packagesOld/Kosa Glina.jpg",
            "imgSingleProduct": "../img/packagesOld/Kosa Glina.jpg",
            "name": "Hair clay",
            "ime": "Glina za kosu",
            "type": "hair",
            "price": 150.00,
            "description": "Handmade oragnic clay with pure essential oil and the finest natural ingredients. This HAIR CLAY is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E.",
            "sastojci": "...",
            "useFor": "hair"
        },
        {
            "id": 1016,
            "imgIndex": "./img/packagesOld/Limun.jpg",
            "imgSingleProduct": "../img/packagesOld/Limun.jpg",
            "name": "Lemon soap",
            "ime": "Sapun sa limunom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This LEMON SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring, lemon enssential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        }
    ]

    return productList;
}