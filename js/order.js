$(document).ready(function () {
    $('#price-total').text(parseFloat(getTotal()).toFixed(2));
});

function getTotal() {
    var total = 0;
    const products = getCartLocalStorage();
    products.forEach(el => {
        const price = el.quantity * el.price;
        total = total + price;
    });

    return total;
};

function getCartLocalStorage() {
    const cart = JSON.parse(localStorage.getItem('cart'));
    return cart;
}

function finishOrder() {
    $('#modal-finish-order').modal('hide');
    window.location.assign('/index.html');
}

function order() {
    console.log('order fnc');

    var validated = isFormValideted()
    if (validated) {
        const form = {
            firstName: $('#firstName').val().trim().toUpperCase(),
            lastName: $('#lastName').val().trim().toUpperCase(),
            email: $('#email').val().trim().toLowerCase(),
            city: $('#city').val().trim().toUpperCase(),
            state: $('#state').val().trim().toUpperCase(),
            zipCode: $('#zipCode').val().trim(),
            address: $('#address').val().trim().toUpperCase(),
            sendPromotion: $('#sendPromotion').is(':checked'),
            termsConditions: $('#termsConditions').is(':checked')
        }
        var cart = JSON.parse(localStorage.getItem('cart'));
        console.log(form);
        $.ajax({
            url: '../mail.php',
            method: 'post',
            data: {
                form: form,
                cart: cart
            },
        })
            .done(async function (data) {
                console.log('DONE', data);
                await removeLocalStorage();
                $('#modal-finish-order').modal('show');
            })
            .fail(function (error) {
                console.log('FAIL', error);
            });
    }
}

function isValidEmailAddress(emailAddress) {
    const pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    return pattern.test(emailAddress);
};

async function removeLocalStorage() {
    console.log('Usao sam u fju');
    await localStorage.removeItem('cartIds');
    await localStorage.removeItem('cart');
}

function isFormValideted() {
    var validated = true;

    const firstName = $('#firstName').val();
    const lastName = $('#lastName').val();
    const email = $('#email').val().toLowerCase();
    const city = $('#city').val();
    const state = $('#state').val();
    const zipCode = $('#zipCode').val();
    const address = $('#address').val();
    const termsConditions = $('#termsConditions').is(':checked');

    if (!firstName) {
        if (!$('#firstName-feedback').length) {
            $('#firstName').parent().append('<p class="error-feedback" id="firstName-feedback">You have to enter first name</p>');
        }
        validated = false;
    } else {
        if ($('#firstName-feedback').length) {
            $('#firstName-feedback').remove();
        }
    }

    if (!lastName) {
        if (!$('#lastName-feedback').length) {
            $('#lastName').parent().append('<p class="error-feedback" id="lastName-feedback">You have to enter last name</p>');
        }
        validated = false;
    } else {
        if ($('#lastName-feedback').length) {
            $('#lastName-feedback').remove();
        }
    }

    if (!email) {
        if (!$('#email-feedback').length) {
            $('#email').parent().append('<p class="error-feedback" id="email-feedback">You have to enter an email</p>');
        }
        validated = false;
    } else {
        if ($('#email-feedback').length) {
            $('#email-feedback').remove();
        }
        if (!isValidEmailAddress(email)) {
            if (!$('#email-feedback').length) {
                $('#email').parent().append('<p class="error-feedback" id="email-feedback">You have to enter valid email</p>');
            }
            validated = false;
        } else {
            if ($('#email-feedback').length) {
                $('#email-feedback').remove();
            }
        }
    }

    if (!city) {
        if (!$('#city-feedback').length) {
            $('#city').parent().append('<p class="error-feedback" id="city-feedback">You have to enter city</p>');
        }
        validated = false;
    } else {
        if ($('#city-feedback').length) {
            $('#city-feedback').remove();
        }
    }

    if (!state) {
        if (!$('#state-feedback').length) {
            $('#state').parent().append('<p class="error-feedback" id="state-feedback">You have to enter state</p>');
        }
        validated = false;
    } else {
        if ($('#state-feedback').length) {
            $('#state-feedback').remove();
        }
    }

    if (!zipCode) {
        if (!$('#zipCode-feedback').length) {
            $('#zipCode').parent().append('<p class="error-feedback" id="zipCode-feedback">You have to enter zip code</p>');
        }
        validated = false;
    } else {
        if ($('#zipCode-feedback').length) {
            $('#zipCode-feedback').remove();
        }

        if (!(!isNaN(zipCode) && zipCode.length == 5)) {
            if (!$('#zipCode-feedback').length) {
                $('#zipCode').parent().append('<p class="error-feedback" id="zipCode-feedback">You have to enter valid zip code</p>');
            }
            validated = false;
        } else {
            if ($('#zipCode-feedback').length) {
                $('#zipCode-feedback').remove();
            }
        }
    }

    if (!address) {
        if (!$('#address-feedback').length) {
            $('#address').parent().append('<p class="error-feedback" id="address-feedback">You have to enter address</p>');
        }
        validated = false;
    } else {
        if ($('#address-feedback').length) {
            $('#address-feedback').remove();
        }
    }

    if (!termsConditions) {
        if (!$('#termsConditions-feedback').length) {
            $('#termsConditions').parent().append('<p class="error-feedback" id="termsConditions-feedback">You have to agree to terms and conditions</p>');
        }
        validated = false;
    } else {
        if ($('#termsConditions-feedback').length) {
            $('#termsConditions-feedback').remove();
        }
    }

    return validated;
}