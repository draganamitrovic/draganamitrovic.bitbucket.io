$(document).ready(function () {
    var productList = document.getElementById('product-list');
    productList.innerHTML = '';
    var products = getAllProducts();

    for (var i = 0; i < products.length; i++) {
        console.log(i)
        var productDiv = document.createElement('div');
        productDiv.classList.add('single-product');

        var productImg = document.createElement('img');
        productImg.src = products[i].imgIndex;
        productImg.setAttribute('alt', products[i].name)

        var productName = document.createElement('p');
        productName.classList.add('single-product-name');
        var name = document.createTextNode(products[i].name.toUpperCase());
        productName.appendChild(name);

        var productPrice = document.createElement('p');
        productPrice.classList.add('single-product-price');
        var price = document.createTextNode(parseFloat(products[i].price).toFixed(2) + " RSD");
        productPrice.appendChild(price);

        var productlink = document.createElement('a');
        productlink.classList.add('cart-link');
        var cartText = document.createTextNode('ADD TO CART');
        productlink.appendChild(cartText);
        productlink.setAttribute('id', "product" + products[i].id);
        productlink.addEventListener("click", function () { addToCart(this.id) }, true);

        productDiv.appendChild(productImg);
        productDiv.appendChild(productName);
        productDiv.appendChild(productPrice);
        productDiv.appendChild(productlink);

        productList.appendChild(productDiv);
    }
});

function setLocalStorage(cartIds, cart) {
    removeLocalStorage()
        .then(
            localStorage.setItem('cartIds', cartIds))
        .then(
            localStorage.setItem('cart', JSON.stringify(cart)))
}

async function removeLocalStorage() {
    return new Promise((resolve, reject) => {
        localStorage.removeItem('cartIds');
        localStorage.removeItem('cart');
    });
}

function getCartIdsLocalStorage() {
    var cartIds = localStorage.getItem('cartIds').split(',');
    return cartIds;
}

function getCartLocalStorage() {
    var cart = JSON.parse(localStorage.getItem('cart'));
    return cart;
}

function notExists(array, element) {
    console.log(array, element);

    var exists = false;
    array.forEach(el => {
        if (el == element) {
            exists = true;
        }
    });
    return !exists;
}

function getNewProduct(id) {
    var newProduct = _.where(getAllProducts(), { id: id });
    var newPrductCart = { id: newProduct[0].id, name: newProduct[0].name, quantity: 1, price: newProduct[0].price };
    return newPrductCart;
}

async function addToCart(idString) {
    var id = Number(idString.substr(-1));
    console.log("addToCart ", idString, id);
    var cart = [];
    var cartIds = [];

    if (localStorage.getItem('cartIds')) {
        if (notExists(getCartIdsLocalStorage(), id)) {
            console.log('exists');
            cartIds = getCartIdsLocalStorage();
            cart = getCartLocalStorage();
            cartIds.push(id);
            cart.push(getNewProduct(id));
            console.log(cartIds, cart);
            setLocalStorage(cartIds, cart);
        }
    } else {
        cartIds.push(id);
        cart.push(getNewProduct(id));
        console.log(cartIds, cart);
        setLocalStorage(cartIds, cart);
    }
}

function getAllProducts() {
    var productList = [
        {
            "id": 1002,
            "imgIndex": "./img/packagesOld/Brusnica.jpg",
            "imgSingleProduct": "../img/packagesOld/Brusnica.jpg",
            "name": "Cranberry soap",
            "ime": "Sapun sa brusnicom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This CRANBERRY SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and cranberry essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1003,
            "imgIndex": "./img/packagesOld/Neven i Aktivni ugalj.jpg",
            "imgSingleProduct": "../img/packagesOld/Neven i Aktivni ugalj.jpg",
            "name": "Calendula with activated charcoal soap",
            "ime": "Sapun sa nevenom i aktivnim ugljem",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This CALENDULA WITH ACTIVATED CHARCOAL SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E and calendula oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1004,
            "imgIndex": "./img/packagesOld/Neven, Zelena glina i Zeolit.jpg",
            "imgSingleProduct": "../img/packagesOld/Neven, Zelena glina i Zeolit.jpg",
            "name": "Calendula with green clay and zeolite soap",
            "ime": "Sapun sa nevenom, zelenom glinom i zeolitom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This CALENDULA WITH GREEN CLAY AND ZEOLITE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and peach essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1005,
            "imgIndex": "./img/packagesOld/Breskva.jpg",
            "imgSingleProduct": "../img/packagesOld/Breskva.jpg",
            "name": "Peach soap",
            "ime": "Sapun sa breskvom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This PEACH SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and peach essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1006,
            "imgIndex": "./img/packagesOld/Lavanda.jpg",
            "imgSingleProduct": "../img/packagesOld/Lavanda.jpg",
            "name": "Lavander soap",
            "ime": "Sapun sa lavandom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This LAVANDER SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and lavander essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1007,
            "imgIndex": "./img/packagesOld/",
            "imgSingleProduct": "../img/packagesOld/",
            "name": "Mint soap",
            "ime": "Sapun sa mentom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This MINT SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and mint essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1008,
            "imgIndex": "./img/packagesOld/Borove iglice.jpg",
            "imgSingleProduct": "../img/packagesOld/Borove iglice.jpg",
            "name": "Pine soap",
            "ime": "Sapun sa borovim iglicama",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This PINE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring and pine essential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1009,
            "imgIndex": "./img/packagesOld/BRada i brkovi sapun.jpg",
            "imgSingleProduct": "../img/packagesOld/BRada i brkovi sapun.jpg",
            "name": "Beard soap",
            "ime": "Sapun za bradu",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This BEARD SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1010,
            "imgIndex": "./img/packagesOld/BRada i brkovi sapun.jpg",
            "imgSingleProduct": "../img/packagesOld/BRada i brkovi sapun.jpg",
            "name": "Beard and moustache soap",
            "ime": "Sapun za bradu i brkove",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This BEARD AND MOUSTACHE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring.",
            "sastojci": "...",
            "useFor": "face, beard, moustache"
        },
        {
            "id": 1011,
            "imgIndex": "./img/packagesOld/BRada i brkovi sapun.jpg",
            "imgSingleProduct": "../img/packagesOld/BRada i brkovi sapun.jpg",
            "name": "Beard soap",
            "ime": "Sapun za bradu",
            "type": "beard",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This BEARD SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring.",
            "sastojci": "...",
            "useFor": "beard"
        },
        {
            "id": 1012,
            "imgIndex": "./img/packagesOld/BRada i brkovi sapun.jpg",
            "imgSingleProduct": "../img/packagesOld/BRada i brkovi sapun.jpg",
            "name": "Beard and moustache soap",
            "ime": "Sapun za bradu i brkove",
            "type": "moustache",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This BEARD AND MOUSTACHE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring.",
            "sastojci": "...",
            "useFor": "moustache"
        },
        {
            "id": 1013,
            "imgIndex": "./img/packagesOld/Brk Sandalovina.jpg",
            "imgSingleProduct": "../img/packagesOld/Brk Sandalovina.jpg",
            "name": "Sandalovina wax",
            "ime": "Vosak sa sandalovinom",
            "type": "moustache",
            "price": 150.00,
            "description": "Handmade oragnic wax with pure essential oil and the finest natural ingredients. This SANDALOVINA WAX is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, sandalovina extract.",
            "sastojci": "...",
            "useFor": "moustache"
        },
        {
            "id": 1014,
            "imgIndex": "./img/packagesOld/Kakao, Cimet, Pomorandza.jpg",
            "imgSingleProduct": "../img/packagesOld/Kakao, Cimet, Pomorandza.jpg",
            "name": "Cocoa, cinnamon, orange soap",
            "ime": "Sapun sa kakaom, cimetom i narandžom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This COCOA, CINNAMON ORANGE SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring, cocoa oil, cinnamon, orange enssential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        },
        {
            "id": 1015,
            "imgIndex": "./img/packagesOld/Kosa Glina.jpg",
            "imgSingleProduct": "../img/packagesOld/Kosa Glina.jpg",
            "name": "Hair clay",
            "ime": "Glina za kosu",
            "type": "hair",
            "price": 150.00,
            "description": "Handmade oragnic clay with pure essential oil and the finest natural ingredients. This HAIR CLAY is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E.",
            "sastojci": "...",
            "useFor": "hair"
        },
        {
            "id": 1016,
            "imgIndex": "./img/packagesOld/Limun.jpg",
            "imgSingleProduct": "../img/packagesOld/Limun.jpg",
            "name": "Lemon soap",
            "ime": "Sapun sa limunom",
            "type": "soap",
            "price": 150.00,
            "description": "Handmade oragnic soap with pure essential oil and the finest natural ingredients. This LEMON SOAP is completely organic and totally free from detergents, alcohol, parabens, silicon and sulphates. Original package weights 90g.",
            "opis": "...",
            "ingredients": "Coconut oil, palm oil, castor oil, almond oil, vitamin E, food coloring, lemon enssential oil.",
            "sastojci": "...",
            "useFor": "face, body and hands"
        }
    ]

    return productList;
}